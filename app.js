const express = require('express');

console.log("Demo Web Server, avec Express");

const usersTab = [
    { id : 1, lastname : 'Beurive',  firstname : 'Aude'},
    { id : 2, lastname : 'Strimelle',  firstname : 'Aurélien'}
];

//Pour créer le serveur
const server = express();

//Pour gérer les routes :
//get : Permet de récupérer 
//post : Pour envoyer des données à la bdd
//delete : Pour supprimer un élément
//put : Pour modifier un élément
//patch : Pour modifier un champs en particulier d'un élément
server.get('/users', (req, res) => {
    const data = {
        users : usersTab
    }
    res.json(data);
});
server.get('/users/:id', (req, res) => {
    //L'objet req possède une propriété params qui est un objet 
    //qui lui même possède une propriété pour chaque paramètre renseigné dans l'url
    const id = req.params.id;
    const data = {
        user : usersTab.find(user => user.id == id)
    }
    res.json(data);
});
server.get('/rutabaga', (req, res) => {
    res.json({ msg : "Oui"});
})

//On le lance sur le port 8080
server.listen(8080, () => {
    console.log('Express Server up on port 8080');
})
